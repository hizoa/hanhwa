package com.hanhwa.di

import android.content.Context
import androidx.room.Room
import com.hanhwa.room.UserDao
import com.hanhwa.room.UserDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object UserModule {

    private val USER_DATABASE_NAME = "user"

    @Singleton
    @Provides
    fun provideUserDatabase(@ApplicationContext context: Context): UserDatabase {
        return Room.databaseBuilder(context, UserDatabase::class.java, USER_DATABASE_NAME).build()
    }

    @Singleton
    @Provides
    fun provideUserDAO(userDb: UserDatabase): UserDao {
        return userDb.getUserDao()
    }
}