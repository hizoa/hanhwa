package com.hanhwa.di

import com.hanhwa.network.ApiClient
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * 깃허브 테스트 api 사용
 * https://docs.github.com/en/rest/reference/users
 **/

@Module //hilt 관련된 모듈이며 이 모듈내의 object나 class는 힐트 관련 정의가 이루어져야 함
@InstallIn(SingletonComponent::class) //hilt 모듈을 설치할 안드로이드 컴포넌트 지정
object NetworkModule {

    private const val BASE_URL = "https://api.github.com"
    private const val TIMEOUT_CONNECT = 10
    private const val TIMEOUT_READ = 10
    private const val TIMEOUT_WRITE = 10

    private val interceptor: HttpLoggingInterceptor by lazy { HttpLoggingInterceptor().apply { setLevel(HttpLoggingInterceptor.Level.BODY) } }
    private val okHttpClient: OkHttpClient by lazy {
        OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .connectTimeout(TIMEOUT_CONNECT.toLong(), TimeUnit.SECONDS)
            .readTimeout(TIMEOUT_READ.toLong(), TimeUnit.SECONDS)
            .writeTimeout(TIMEOUT_WRITE.toLong(), TimeUnit.SECONDS)
            .build()
        }

    @Singleton //hilt로 제공하는 컴포넌트가 스택에 저장되어 앱이 꺼질때까지 유효
    @Provides //함수가 인스턴스를 제공하는 함수임을 hilt에게 알려줌.
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
    }

    @Singleton
    @Provides
    fun provideUserApiClient(retrofit: Retrofit): ApiClient {
        return retrofit.create(ApiClient::class.java)
    }

}