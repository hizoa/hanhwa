package com.hanhwa.base

import androidx.multidex.MultiDexApplication
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
open class TimeFeedApp: MultiDexApplication() {
}