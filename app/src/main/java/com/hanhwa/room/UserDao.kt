package com.hanhwa.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUsers(users: List<RoomUser>): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: RoomUser): Completable

    @Query("DELETE FROM users WHERE id = :id")
    fun deleteUser(id: Int): Completable

    @Query("SELECT * FROM users")
    fun getUserList(): Flowable<List<RoomUser>>

}