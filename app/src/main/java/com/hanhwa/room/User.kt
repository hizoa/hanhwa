package com.hanhwa.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "users")
data class RoomUser(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: Int = 0,
    @ColumnInfo(name = "login")
    val login: String = "",
    @ColumnInfo(name = "url")
    val url: String = "",
    @ColumnInfo(name = "image")
    val image: String = "",
    @ColumnInfo(name = "reply")
    val score: String= "",
    @ColumnInfo(name = "like")
    val like: Int = 0
)