package com.hanhwa.room

import android.service.autofill.UserData
import io.reactivex.Completable
import io.reactivex.Flowable
import javax.inject.Inject

class UserRepository @Inject constructor(
    private val userDao: UserDao
){
    fun insertUsers(users: List<RoomUser>): Completable {
        return userDao.insertUsers(users)
    }

    fun getUsers(): Flowable<List<RoomUser>> {
        return userDao.getUserList()
    }
}