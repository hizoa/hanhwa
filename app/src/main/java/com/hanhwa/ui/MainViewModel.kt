package com.hanhwa.ui

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.hanhwa.network.UserService
import com.hanhwa.room.RoomUser
import com.hanhwa.room.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repository: UserRepository,
    private val service:UserService): ViewModel(){

    val TAG = this::class.java.simpleName
    var loadingLive: MutableLiveData<Boolean> = MutableLiveData()

    fun onCreate() {
        checkDb()
        getUsers()
    }

    private fun getUsers() {
        service.getUsers()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loadingLive.postValue(false)
                var users = ArrayList<RoomUser>()
                it.forEach { user->
                    users.add(RoomUser(user.id, user.login, user.avatar_url, user.url, "", 0 ))
                }
                insertDb(users)
                checkDb()
            },{
                Log.e(TAG, "검색 오류")
                loadingLive.postValue(false)
            })
    }

    private fun insertDb(users: ArrayList<RoomUser>) {
        repository.insertUsers(users)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Log.e(TAG, "저장 성공")
            }, {
                Log.e(TAG, "저장 실패")
            })

    }

    private fun checkDb() {
        repository.getUsers()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                       var a = 0
            }, {

            })
    }
}