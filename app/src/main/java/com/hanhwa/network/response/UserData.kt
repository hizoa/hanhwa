package com.hanhwa.network.response

import java.io.Serializable

class UserData: Serializable {
    var login: String = ""
    var id:Int = 0
    var avatar_url: String = ""
    var url: String = ""
    //좋아요
    var like: Int = 0
}