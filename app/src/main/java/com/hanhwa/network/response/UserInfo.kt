package com.hanhwa.network.response

import com.google.gson.annotations.SerializedName

class UserInfo(
    var items: List<UserData>? = null
)