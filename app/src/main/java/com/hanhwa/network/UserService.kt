package com.hanhwa.network

import com.hanhwa.network.response.UserData
import com.hanhwa.network.response.UserInfo
import io.reactivex.Single
import javax.inject.Inject

class UserService @Inject constructor(private val api:ApiClient) {

    fun getUsers(): Single<List<UserData>> {
        val requestType = mutableMapOf<String, String>()
        return api.getUsers(requestType)
    }

}