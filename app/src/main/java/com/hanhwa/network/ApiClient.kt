package com.hanhwa.network

import com.hanhwa.network.response.UserData
import com.hanhwa.network.response.UserInfo
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface ApiClient {
    @GET("users")
    fun getUsers(@QueryMap query: Map<String, String>): Single<List<UserData>>
}