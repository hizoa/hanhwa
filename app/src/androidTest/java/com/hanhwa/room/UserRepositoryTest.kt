package com.hanhwa.room

import android.content.Context
import androidx.test.platform.app.InstrumentationRegistry
import com.hanhwa.di.UserModule
import org.junit.Assert.*

import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class UserRepositoryTest {

    @Before
    fun setUp() {

    }

    @After
    fun tearDown() {
    }

    @Test
    fun insertUsers() {
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        var db = UserModule.provideUserDAO(UserModule.provideUserDatabase(appContext))
        var result = db.insertUser(RoomUser(0, "a", "b", "c", true ))
        result.subscribe({
            assertTrue(true)
        }, {
            assertFalse(true)
        })
    }
}